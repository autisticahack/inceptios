//
//  TestItemModel.swift
//  NovusSpem
//
//  Created by Mihai Betej on 10/28/16.
//  Copyright © 2016 DB Global Technology. All rights reserved.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import Foundation
import UIKit

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - TestItemModel

func ==(lhs: TestItemModel, rhs: TestItemModel) -> Bool {
    return lhs.getName() == rhs.getName()
}

class TestItemModel: Equatable, NSCoding {
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Vars
    // Name
    private var name: String
    private let nameKey = "test-item-name"
    // Image
    private var image: UIImage
    private let imageKey = "test-item-image"
    // Sound
    private var sound: NSData
    private let soundKey = "test-item-sound"
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Lifecycle
    
    init(name: String, image: UIImage, sound: NSData) {
        self.name = name
        self.image = image
        self.sound = sound
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.name = (aDecoder.decodeObject(forKey: nameKey) as? String) ?? ""
        if let imageData = aDecoder.decodeObject(forKey: imageKey) as? NSData {
            if let image = UIImage(data: imageData as Data) {
                self.image = image
            } else {
                self.image = UIImage()
            }
        } else {
            self.image = UIImage()
        }
        if let soundData = aDecoder.decodeObject(forKey: soundKey) as? NSData {
            self.sound = soundData
        } else {
            self.sound = NSData()
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: nameKey)
        aCoder.encode(UIImagePNGRepresentation(self.image), forKey: imageKey)
        aCoder.encode(sound, forKey: soundKey)
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Accessors
    
    func getName() -> String {
        return name
    }
    
    func getImage() -> UIImage {
        return image
    }
    
    func getSound() -> NSData {
        return sound
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
