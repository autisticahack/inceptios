//
//  GraphItem.swift
//  RedAlert
//
//  Created by Radu Dan on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import Foundation

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - GraphItemModel

struct GraphItemModel {
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK : Vars
    
    var time: Date?
    var feelingsCount: Int?
    var isPositive: Bool?
    var order: Int?
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK : Methods
    
    func chartPoint() -> ChartPoint? {
        guard let time = self.time, let feelingsCount = self.feelingsCount, let order = self.order else {
            return nil
        }
        let xAxisValue = ChartAxisValueString(GraphConstants.dateFormatter.string(from: time), order: order, labelSettings: GraphConstants.labelSettings)
        let yAxisValue = ChartAxisValueInt(feelingsCount)
        
        return ChartPoint(x: xAxisValue, y: yAxisValue)
    }
    
    static func graphResponseModelFromDictionary(dictionary: [String: Any]) -> [GraphServerResponseModel]? {
        guard let embeddedDictionary = (dictionary["_embedded"] as? [String: Any]) else {
            return nil
        }
        if let entries = embeddedDictionary["entries"] as? [[String: Any]] {
            var graphEntries: [GraphServerResponseModel] = []
            for entryDic in entries {
                var graphEntry = GraphServerResponseModel()
                if let timestamp = entryDic["timestamp"] as? String {
                    graphEntry.timestamp = timestamp
                }
                if let emotion = entryDic["emotion"] as? String {
                    graphEntry.emotion = emotion
                }
                if let action = entryDic["action"] as? String {
                    graphEntry.action = action
                }
                if let latitude = entryDic["latitude"] as? String {
                    graphEntry.latitude = latitude
                }
                if let longitude = entryDic["longitude"] as? String {
                    graphEntry.longitude = longitude
                }
                if let negative = entryDic["negative"] as? Bool {
                    graphEntry.negative = negative
                }
                if let better = entryDic["better"] as? Bool {
                    graphEntry.better = better
                }
                graphEntries.append(graphEntry)
            }
            return graphEntries
        }
        return nil
    }
    
    static func fromDictionary(dictionary: [String: Any]) -> (positive: [GraphItemModel], negative: [GraphItemModel])? {
        guard let embeddedDictionary = (dictionary["_embedded"] as? [String: Any]) else {
            return nil
        }
        if let entries = embeddedDictionary["entries"] as? [[String: Any]] {
            var graphEntries: [GraphServerResponseModel] = []
            for entryDic in entries {
                var graphEntry = GraphServerResponseModel()
                if let timestamp = entryDic["timestamp"] as? String {
                    graphEntry.timestamp = timestamp
                }
                if let emotion = entryDic["emotion"] as? String {
                    graphEntry.emotion = emotion
                }
                if let action = entryDic["action"] as? String {
                    graphEntry.action = action
                }
                if let latitude = entryDic["latitude"] as? String {
                    graphEntry.latitude = latitude
                }
                if let longitude = entryDic["longitude"] as? String {
                    graphEntry.longitude = longitude
                }
                if let negative = entryDic["negative"] as? Bool {
                    graphEntry.negative = negative
                }
                if let better = entryDic["better"] as? Bool {
                    graphEntry.better = better
                }
                graphEntries.append(graphEntry)
            }
            var outputDictionary: [String: GraphValueModel] = [:]
            for graphEntry in graphEntries {
                guard let negative = graphEntry.negative, let timestamp = graphEntry.timestamp else {
                    continue
                }
                if let graphModel = outputDictionary[timestamp] {
                    let negativeCount = graphModel.negativeCount + (negative ? 1 : 0)
                    let positiveCount = graphModel.positiveCount + (negative ? 0 : 1)
                    let newGraphModel = GraphValueModel(negativeCount: negativeCount, positiveCount: positiveCount)
                    outputDictionary[timestamp] = newGraphModel
                }
                else {
                    let negativeCount = negative ? 1 : 0
                    let positiveCount = negative ? 0 : 1
                    let graphModel = GraphValueModel(negativeCount: negativeCount, positiveCount: positiveCount)
                    outputDictionary[timestamp] = graphModel
                }
            }
            var positiveModels: [GraphItemModel] = []
            var negativeModels: [GraphItemModel] = []
            var order = 0
            for (key, value) in outputDictionary {
                order += 2
                let date = GraphConstants.serverDateFormatter.date(from: key)
                let posModel = GraphItemModel(time: date, feelingsCount: value.positiveCount, isPositive: true, order: order)
                positiveModels.append(posModel)
                let negModel = GraphItemModel(time: date, feelingsCount: value.negativeCount, isPositive: false, order: order)
                negativeModels.append(negModel)
                
            }
            return (positive: positiveModels, negative: negativeModels)
        }
        return nil
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - GraphServerResponseModel

struct GraphServerResponseModel {
    var timestamp: String?
    var emotion: String?
    var action: String?
    var latitude: String?
    var longitude: String?
    var negative: Bool?
    var better: Bool?
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - GraphValueModel

struct GraphValueModel {
    var negativeCount: Int = 0
    var positiveCount: Int = 0
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
