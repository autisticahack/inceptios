//
//  HeartRateMonitor.swift
//  RedAlert
//
//  Created by ANDREEA LAVINIA  on 10/11/2016.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit
import HealthKit

class HeartRateMonitor: NSObject {
    
    var healtKitHeartRates = [Double]()
    var firstTimeRate: Bool = false
    let healthStore = HKHealthStore()
    let heartRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.heartRate)!
//    let respiratoryRateType = HKQuantityType.quantityType(forIdentifier: HKQuantityTypeIdentifier.respiratoryRate)!

    func retreiveHeartRateDataFromHealthKit(completion: (([Double]?) -> Void)) {
        
        if (HKHealthStore.isHealthDataAvailable()) {
            var csvString = "Time,Date,Rate(BPM)\n"
            self.healthStore.requestAuthorization(toShare: nil, read:[self.heartRateType], completion:{(success, error) in
                let sortByTime = NSSortDescriptor(key:HKSampleSortIdentifierEndDate, ascending:false)
                let timeFormatter = DateFormatter()
                timeFormatter.dateFormat = "hh:mm:ss"
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "MM/dd/YYYY"
                self.healtKitHeartRates.removeAll()
                
                let heartQuery = HKSampleQuery(sampleType:self.heartRateType, predicate:nil, limit:400, sortDescriptors:[sortByTime], resultsHandler:{(query, results, error) in
                    guard let results = results else {
                        return
                    }
                    
                    self.healtKitHeartRates.removeAll()
                    for quantitySample in results {
                        
                        let quantity = (quantitySample as! HKQuantitySample).quantity
                        let heartRateUnit = HKUnit(from: "count/min")
                        
                        //                        csvString.extend("\(quantity.doubleValueForUnit(heartRateUnit)),\(timeFormatter.stringFromDate(quantitySample.startDate)),\(dateFormatter.stringFromDate(quantitySample.startDate))\n")
                        //                        println("\(quantity.doubleValueForUnit(heartRateUnit)),\(timeFormatter.stringFromDate(quantitySample.startDate)),\(dateFormatter.stringFromDate(quantitySample.startDate))")
                        csvString += "\(timeFormatter.string(from: quantitySample.startDate)),\(dateFormatter.string(from: quantitySample.startDate)),\(quantity.doubleValue(for: heartRateUnit))\n"
                        //                        print("\(timeFormatter.string(from: quantitySample.startDate)),\(dateFormatter.string(from: quantitySample.startDate)),\(quantity.doubleValue(for: heartRateUnit))")
                        //print("De la health ultimele\(quantity.doubleValue(for: heartRateUnit))")
                        self.healtKitHeartRates.append(quantity.doubleValue(for: heartRateUnit))
                    }
                    
                    do {
                        let documentsDir = try FileManager.default.url(for: .documentDirectory, in:.userDomainMask, appropriateFor:nil, create:true)
                        try csvString.write(to: URL(string:"HeartRateData.csv", relativeTo:documentsDir)!, atomically:true, encoding:String.Encoding.ascii)
            
                    }
                    catch {
                        print("Error occured")
                    }
                    
                })
                self.healthStore.execute(heartQuery)
            })
        }
        completion(self.healtKitHeartRates)
    }
    
    func setUpBackgroundDelivery() {
        // Heart Rate Type
        let heartQuery = HKObserverQuery(sampleType: self.heartRateType, predicate: nil) { [weak self] (query: HKObserverQuery, completionHandler: HKObserverQueryCompletionHandler, error: Error?) in
            if error != nil {
                // Perform Proper Error Handling Here...
                print("*** An error occured while setting up the heartRate observer. \(error?.localizedDescription) ***")
            }
            
            guard let strongSelf = self else { return }
            strongSelf.retreiveHeartRateDataFromHealthKit(completion: { (results) in
                
            })
            completionHandler()
        }
        self.healthStore.execute(heartQuery)
        self.healthStore.enableBackgroundDelivery(for: self.heartRateType, frequency: .immediate) { (success: Bool, error: Error?) in
            print("Enable Heart Rate Data Background Delivery Success: \(success)")
        }
    }

}
