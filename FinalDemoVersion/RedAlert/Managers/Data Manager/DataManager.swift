//
//  DataManager.swift
//  NovusSpem
//
//  Created by Mihai Betej on 10/28/16.
//  Copyright © 2016 DB Global Technology. All rights reserved.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import Foundation
import UIKit
import MediaPlayer

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK : DataManager

class DataManager {
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK : Vars
    
    /// Stored files names
    private let storedTestItemsFileName = "novusspemtestitems.dat"
    private let storedMediaItemsFileName = "novusspemmediaitems.dat"
    
    /// Stored files paths in the user directory
    private var storedTestItemsFilePath: String {
        get {
            return FileManager.getDocumentsDirectory().appendingPathComponent(storedTestItemsFileName).absoluteString
        }
    }
    private var storedMediaItemsFilePath: String {
        get {
            return FileManager.getDocumentsDirectory().appendingPathComponent(storedMediaItemsFileName).absoluteString
        }
    }
    
    /// Added test items
    private lazy var testItems: Array<TestItemModel> = {
        var tempTestItems = Array<TestItemModel>()
        if let savedTestItems = NSKeyedUnarchiver.unarchiveObject(withFile: self.storedTestItemsFilePath) as? Array<TestItemModel> {
            tempTestItems.append(contentsOf: savedTestItems)
        }
        
        return tempTestItems
    }()
    
    private lazy var mediaItems: Array<MPMediaItem> = {
        var tempMediaItems = Array<MPMediaItem>()
        if let savedMediaItems = NSKeyedUnarchiver.unarchiveObject(withFile: self.storedMediaItemsFilePath) as? Array<MPMediaItem> {
            tempMediaItems.append(contentsOf: savedMediaItems)
        }
        
        return tempMediaItems
    }()
    
    /// Singleton
    static let sharedInstance = DataManager()
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK : Test items
    
    private func saveTestItems() {
        NSKeyedArchiver.archiveRootObject(testItems, toFile: storedTestItemsFilePath)
    }
    
    
    func addTestItem(name: String, image: UIImage, sound: NSData) {
        testItems.append(TestItemModel(name: name, image: image, sound: sound))
        saveTestItems()
    }
    
    func removeTestItem(at index: Int) {
        if index < testItems.count {
            testItems.remove(at: index)
            saveTestItems()
        }
    }
    
    func remove(_ testItem: TestItemModel) {
        if let index = testItems.index(of: testItem) {
            removeTestItem(at: index)
        }
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK : Media items
    
    private func saveMediaItems() {        
        NSKeyedArchiver.archiveRootObject(mediaItems, toFile: storedMediaItemsFilePath)
    }
    
    func addMediaItems(newMediaItems: [MPMediaItem]) {
        mediaItems.append(contentsOf: newMediaItems)
        saveMediaItems()
    }
    
    func getMediaItems() -> Array<MPMediaItem> {
        return mediaItems
    }

    func mediaItem(for itemWithAVURLAsset: AVURLAsset) -> MPMediaItem {
        let filteredMI = mediaItems.filter {
            let asset = AVURLAsset(url: $0.assetURL!, options: nil)
            return asset.url == itemWithAVURLAsset.url
        }
        
        return filteredMI.first!
    }
    
    func nextMediaItem(after itemWithAVURLAsset: AVURLAsset) -> MPMediaItem {
        let filteredMI = mediaItems.filter {
            let asset = AVURLAsset(url: $0.assetURL!, options: nil)
            return asset.url == itemWithAVURLAsset.url
        }
        
        let currentMI = filteredMI.first!
        var currentMIIndex = mediaItems.index(of: currentMI)
        if currentMIIndex == nil {
            currentMIIndex = -1
        }
        
        var nextMIIndex = currentMIIndex! + 1
        if nextMIIndex >= mediaItems.count {
            nextMIIndex = 0
        }
        
        return mediaItems[nextMIIndex]
    }
    
    func prevMediaItem(before itemWithAVURLAsset: AVURLAsset) -> MPMediaItem {
        let filteredMI = mediaItems.filter {
            let asset = AVURLAsset(url: $0.assetURL!, options: nil)
            return asset.url == itemWithAVURLAsset.url
        }
        
        let currentMI = filteredMI.first!
        var currentMIIndex = mediaItems.index(of: currentMI)
        if currentMIIndex == nil {
            currentMIIndex = 0
        }
        
        var prevMIIndex = currentMIIndex! - 1
        if prevMIIndex < 0 {
            prevMIIndex = mediaItems.count - 1
        }
        
        return mediaItems[prevMIIndex]
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
