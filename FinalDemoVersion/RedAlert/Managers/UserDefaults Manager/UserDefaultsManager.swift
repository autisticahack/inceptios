//
//  UserDefaultsManager.swift
//  RedAlert
//
//  Created by Florin Voicu on 11/10/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import Foundation

public struct DefaultsKeys {
    static let selectedEmotionKey = "selectedEmotionKey"
}

class Settings {
    static let userDefaults = UserDefaults.standard
    
    static func write(value: Any, forKey: String){
        Settings.userDefaults.set(value, forKey: forKey)
        Settings.userDefaults.synchronize()
    }
    
    static func remove(forKey: String) {
        Settings.userDefaults.removeObject(forKey: forKey)
        Settings.userDefaults.synchronize()
    }
    
    static func value(forKey: String) -> Any? {
        return Settings.userDefaults.value(forKey: forKey)
    }
}
