//
//  MainTabBarControllerViewController.swift
//  RedAlert
//
//  Created by Florin Voicu on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit

class MainTabBarControllerViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBar.barTintColor = UIColor.white
        self.tabBar.tintColor = BaseColors.lightViolet

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var selectedIndex: Int{
        didSet{
            switch selectedIndex {
            case 0:
                self.tabBar.tintColor = BaseColors.lightVioletGradientBottom
            case 1:
                self.tabBar.tintColor = BaseColors.darkViolet
            case 2:
                self.tabBar.tintColor = BaseColors.green
            default:
                break
            }
        }
    }
    
    // MARK: - Tab bar delegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        Settings.remove(forKey: DefaultsKeys.selectedEmotionKey)
        switch item.tag {
        case 1000:
            self.tabBar.tintColor = BaseColors.lightVioletGradientBottom
        case 1001:
            self.tabBar.tintColor = BaseColors.darkViolet
        case 1002:
            self.tabBar.tintColor = BaseColors.green
        default:
            break
        }
    }
    
}
