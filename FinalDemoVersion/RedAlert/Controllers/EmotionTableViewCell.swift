//
//  EmotionTableViewCell.swift
//  RedAlert
//
//  Created by Florin Voicu on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit

class EmotionTableViewCell: UITableViewCell {
    //outlets
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var emotionText: UILabel!
    
    //class properties
    var emotion: String?
    
    func configureCell(withEmotion:String?){
        if let emotion = withEmotion{
            self.emotion = emotion
            //emotionText.text = NSLocalizedString(emotion, comment: "no_emotion_found")
            let image = UIImage(named: emotion)
            backgroundImage.image = image
        } else{
            //emotionText.text = NSLocalizedString("no_emotion_found", comment: "no_emotion_found")
        }
    }

}
