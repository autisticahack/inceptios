//
//  GraphViewController.swift
//  RedAlert
//
//  Created by Radu Dan on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import UIKit

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: -  GraphViewController

class GraphViewController: UIViewController {
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK : Vars
    
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private var chart: Chart?
    private var gradientWasAdded: Bool = false
    
    private var positiveItems: [GraphItemModel] = [] {
        didSet {
            positiveChartPoints = []
            positiveChartPoints = positiveItems.map({$0.chartPoint()!})
        }
    }
    private var positiveChartPoints: [ChartPoint] = []
    
    private var negativeItems: [GraphItemModel] = [] {
        didSet {
            negativeChartPoints = []
            negativeChartPoints = negativeItems.map({$0.chartPoint()!})
        }
    }
    private var negativeChartPoints: [ChartPoint] = []
    
    var allItems: [GraphServerResponseModel] = []
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK : Methods

    override func viewDidLoad() {
        super.viewDidLoad()

        getItems()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        customizeGradient()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customizeGradient() {
        guard !gradientWasAdded else {
            return
        }
        let gradientLayer = CAGradientLayer()
        let frame = topContainer.frame
        gradientLayer.frame = frame
        gradientLayer.colors = [BaseColors.darkVioletGradientTop.cgColor, BaseColors.darkVioletGradientBottom.cgColor]
        gradientLayer.locations = [0, 1]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        topContainer.layer.insertSublayer(gradientLayer, at: 0)
        
        segmentedControl.layer.cornerRadius = 5.0
        segmentedControl.layer.borderColor = UIColor.white.cgColor
        segmentedControl.layer.backgroundColor = UIColor.white.cgColor
        segmentedControl.layer.borderWidth = 1.0
        segmentedControl.layer.masksToBounds = true
        
        gradientWasAdded = true
    }
    
    private func addGraph() {
        
        let allChartPoints = (positiveChartPoints + negativeChartPoints).sorted {(obj1, obj2) in return obj1.x.scalar < obj2.x.scalar}
        
        let xValues: [ChartAxisValue] = (NSOrderedSet(array: allChartPoints).array as! [ChartPoint]).map{$0.x}
        let yValues = ChartAxisValuesGenerator.generateYAxisValuesWithChartPoints(allChartPoints, minSegmentCount: 1, maxSegmentCount: 10, multiple: 2, axisValueGenerator: {ChartAxisValueDouble($0, labelSettings: GraphConstants.labelSettings)}, addPaddingSegmentIfEdge: false)
        
        let xModel = ChartAxisModel(axisValues: xValues, axisTitleLabel: ChartAxisLabel(text: "Date", settings: GraphConstants.labelSettings))
        let yModel = ChartAxisModel(axisValues: yValues, axisTitleLabel: ChartAxisLabel(text: "Feelings", settings: GraphConstants.labelSettings.defaultVertical()))
        let chartFrame = CGRect(x: 0, y: 22, width: view.bounds.width, height: 150)//GraphConstants.chartFrame(view.bounds)
        let chartSettings = GraphConstants.chartSettings
        chartSettings.trailing = 20
        chartSettings.labelsToAxisSpacingX = 20
        chartSettings.labelsToAxisSpacingY = 20
        let coordsSpace = ChartCoordsSpaceLeftBottomSingleAxis(chartSettings: chartSettings, chartFrame: chartFrame, xModel: xModel, yModel: yModel)
        let (xAxis, yAxis, innerFrame) = (coordsSpace.xAxis, coordsSpace.yAxis, coordsSpace.chartInnerFrame)
        
        let c1 = UIColor(red: 14.0/255.0, green: 191.0/255.0, blue: 181.0/255.0, alpha: 0.4)
        let c2 = UIColor.clear//UIColor(red: 82.0/255.0, green: 72.0/255.0, blue: 107.0/255.0, alpha: 0.4)
        
        let chartPointsLayer1 = ChartPointsAreaLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: positiveChartPoints, areaColor: c1, animDuration: 3, animDelay: 0, addContainerPoints: true)
        let chartPointsLayer2 = ChartPointsAreaLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: negativeChartPoints, areaColor: c2, animDuration: 3, animDelay: 0, addContainerPoints: true)
        
        let lineModel1 = ChartLineModel(chartPoints: positiveChartPoints, lineColor: BaseColors.green, animDuration: 1, animDelay: 0)
        let lineModel2 = ChartLineModel(chartPoints: negativeChartPoints, lineColor: BaseColors.lightViolet, animDuration: 1, animDelay: 0)
        
        let chartPointsLineLayer = ChartPointsLineLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, lineModels: [lineModel1, lineModel2])
        
        var popups: [UIView] = []
        var selectedView: ChartPointTextCircleView?
        
        let circleViewGenerator = {(chartPointModel: ChartPointLayerModel, layer: ChartPointsLayer, chart: Chart) -> UIView? in
            
            let (chartPoint, screenLoc) = (chartPointModel.chartPoint, chartPointModel.screenLoc)
            
            let v = ChartPointTextCircleView(chartPoint: chartPoint, center: screenLoc, diameter: Env.iPad ? 50 : 15, cornerRadius: Env.iPad ? 24: 8, borderWidth: Env.iPad ? 2 : 1, font: GraphConstants.fontWithSize(Env.iPad ? 14 : 8))
            v.viewTapped = {view in
                for p in popups {p.removeFromSuperview()}
                selectedView?.selected = false
                
                let w: CGFloat = Env.iPad ? 250 : 150
                let h: CGFloat = Env.iPad ? 100 : 80
                
                let x: CGFloat = {
                    let attempt = screenLoc.x - (w/2)
                    let leftBound: CGFloat = chart.bounds.origin.x
                    let rightBound = chart.bounds.size.width - 5
                    if attempt < leftBound {
                        return view.frame.origin.x
                    } else if attempt + w > rightBound {
                        return rightBound - w
                    }
                    return attempt
                }()
                
                let frame = CGRect(x: x, y: screenLoc.y - (h + (Env.iPad ? 30 : 12)), width: w, height: h)
                
                let bubbleView = InfoBubble(frame: frame, arrowWidth: Env.iPad ? 40 : 28, arrowHeight: Env.iPad ? 20 : 14, bgColor: UIColor.black, arrowX: screenLoc.x - x)
                chart.addSubview(bubbleView)
                
                bubbleView.transform = CGAffineTransform(scaleX: 0, y: 0).concatenating(CGAffineTransform(translationX: 0, y: 100))
                let infoView = UILabel(frame: CGRect(x: 0, y: 10, width: w, height: h - 30))
                infoView.textColor = UIColor.white
                infoView.backgroundColor = UIColor.black
                infoView.text = "N° of feelings: \(chartPoint.y)"
                infoView.font = GraphConstants.fontWithSize(Env.iPad ? 14 : 12)
                infoView.textAlignment = NSTextAlignment.center
                
                bubbleView.addSubview(infoView)
                popups.append(bubbleView)
                
                UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions(), animations: {
                    view.selected = true
                    selectedView = view
                    
                    bubbleView.transform = CGAffineTransform.identity
                    }, completion: {finished in})
            }
            
            return v
        }
        
        let itemsDelay: Float = 0.08
        let chartPointsCircleLayer1 = ChartPointsViewsLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: positiveChartPoints, viewGenerator: circleViewGenerator, displayDelay: 0.9, delayBetweenItems: itemsDelay)
        
        let chartPointsCircleLayer2 = ChartPointsViewsLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, chartPoints: negativeChartPoints, viewGenerator: circleViewGenerator, displayDelay: 1.8, delayBetweenItems: itemsDelay)
        
        let settings = ChartGuideLinesDottedLayerSettings(linesColor: UIColor.black, linesWidth: GraphConstants.guidelinesWidth)
        let guidelinesLayer = ChartGuideLinesDottedLayer(xAxis: xAxis, yAxis: yAxis, innerFrame: innerFrame, settings: settings)
        
        let chart = Chart(
            frame: chartFrame,
            layers: [
                xAxis,
                yAxis,
                guidelinesLayer,
                chartPointsLayer1,
                chartPointsLayer2,
                chartPointsLineLayer,
                chartPointsCircleLayer1,
                chartPointsCircleLayer2
            ]
        )
        
        topContainer.addSubview(chart.view)
        self.chart = chart
    }
    
    private func getItems() {
        
        if let items = GraphItemModel.fromDictionary(dictionary: mockJSON()!) {
            positiveItems = items.positive
            negativeItems = items.negative
            addGraph()    
        }
        if let allItems = GraphItemModel.graphResponseModelFromDictionary(dictionary: mockJSON()!) {
            self.allItems = allItems
            tableView.reloadData()
        }
 
        /*
        let url = try! "http://10.0.179.14:8080/entries".asURL()
        let networkManager = NetworkManager()
        networkManager.makeJSONRequest(url) { (response) in
            if let resultJSON = response.result.value as? Dictionary<String, Any> {
                if let items = GraphItemModel.fromDictionary(dictionary: resultJSON) {
                    self.positiveItems = items.positive
                    self.negativeItems = items.negative
                    self.addGraph()
                }
                if let allItems = GraphItemModel.graphResponseModelFromDictionary(dictionary: resultJSON) {
                    self.allItems = allItems
                    self.tableView.reloadData()
                }
            }
        }*/
    }
    
    
    
    private func mockJSON() -> Dictionary<String, Any>? {
        guard let path = Bundle.main.path(forResource: "mockup-entries", ofType: "json"), let data = NSData(contentsOfFile: path) else {
            return nil
        }
        do {
            return try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as? Dictionary<String, Any>
        } catch {}
        
        return nil
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MapSegueId" {
            let indexPath = tableView.indexPathForSelectedRow
            let item = allItems[indexPath!.row]
            if let vc = segue.destination as? MapViewController {
                vc.latitude = item.latitude
                vc.longitude = item.longitude
                vc.emotion = item.emotion?.uppercased()
                vc.negative = item.negative
            }
        }
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func onTouchClose(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: -  UITableViewDataSource

extension GraphViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "GraphTableViewCell", for: indexPath) as? GraphTableViewCell else {
            return UITableViewCell()
        }
        let item = allItems[indexPath.row]
        let date = GraphConstants.serverDateFormatter.date(from: item.timestamp!)
        cell.loadData(date: date!, emotion: item.emotion!, isNegative: item.negative!)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor(red: 238.0/255.0, green: 238.0/255.0, blue: 238.0/255.0, alpha: 1.0)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cellToDeSelect:UITableViewCell = tableView.cellForRow(at: indexPath)!
        cellToDeSelect.contentView.backgroundColor = UIColor.clear
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
