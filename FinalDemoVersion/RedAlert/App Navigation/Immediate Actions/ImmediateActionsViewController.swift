//
//  ImmediateActionsViewController.swift
//  RedAlert
//
//  Created by Mihai Betej on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import UIKit

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class ImmediateActionsViewController: UIViewController {

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Vars
    
    @IBOutlet var immediateActionButtons: [UIButton]!
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Relax"
        self.customizeAppearance()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Class utils
    
    class func showImmediateActions(presentingController: UIViewController? = nil) {
        // Detect action based on current emotion
        let currentEmotion = Settings.value(forKey: DefaultsKeys.selectedEmotionKey)
        guard let action = Action.getEmotionActions(emotion: currentEmotion as! String).first else {
            return
        }

        // Show navigation relevant for this action
        self.show(action: action)
    }
    
    class func show(action: Action, presentingController: UIViewController? = nil) {
        // Load relevant Storyboard and prepare for navigation
        let immediateActionsStoryboard = UIStoryboard(name: "ImmediateActionsStoryboard", bundle: nil)
        var initialViewController: UIViewController?
        var navController: UINavigationController?
        
        // Determine navigation based on received action
        switch action {
        case Action.game:
            initialViewController = immediateActionsStoryboard.instantiateViewController(withIdentifier: "PlayViewController")
            navController = UINavigationController(rootViewController: initialViewController!)
        case Action.deepBreathing:
            initialViewController = immediateActionsStoryboard.instantiateViewController(withIdentifier: "BreatheViewController")
            navController = UINavigationController(rootViewController: initialViewController!)
            navController!.setNavigationBarHidden(true, animated: false)
        case Action.listenMusic:
            initialViewController = immediateActionsStoryboard.instantiateViewController(withIdentifier: "MusicViewController")
            navController = UINavigationController(rootViewController: initialViewController!)
        default:
            initialViewController = immediateActionsStoryboard.instantiateViewController(withIdentifier: "ComingSoonViewController")
            navController = UINavigationController(rootViewController: initialViewController!)
            navController!.setNavigationBarHidden(true, animated: false)
        }
        
        // Check if we have a presenter
        var presenter: UIViewController? = nil
        if presentingController == nil {
            if let rootVC = UIApplication.shared.keyWindow?.rootViewController {
                presenter = rootVC
            }
        } else {
            presenter = presentingController!
        }
        guard let _presenter = presenter else {
            return
        }
        
        // If we do have a presenter, proceed with navigation
        if navController != nil {
            _presenter.present(navController!, animated: true, completion: nil)
        }
    }
    
    private func customizeAppearance() {
        self.view.backgroundColor = UIColor.randomColor()        
        for immediateActionButton in immediateActionButtons {
            immediateActionButton.setBackgroundImage(UIImage(color: UIColor.randomColor(), size: immediateActionButton.frame.size), for: .normal)
            immediateActionButton.setTitleColor(UIColor.white, for: .normal)
        }
        
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Actions
    
    @IBAction func doneAction(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
