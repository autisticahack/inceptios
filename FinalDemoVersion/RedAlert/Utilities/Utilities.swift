//
//  Utilities.swift
//  NovusSpem
//
//  Created by Mihai Betej on 10/28/16.
//  Copyright © 2016 DB Global Technology. All rights reserved.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import Foundation
import UIKit

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - Extensions

extension FileManager {
    
    static func getDocumentsDirectory() -> URL {
        let paths = self.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths.first!
        return documentsDirectory
    }
    
}

extension String {
    
    /**
     A function which returns a Base64 encoded string
     
     - Returns: A Base64 encoded string
     
     */
    
    func base64Encode() -> String? {
        if let inputData = self.data(using: String.Encoding.utf8){
            return inputData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
        }
        
        return nil
    }
    
}

extension UIImage {
    
    convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }

    convenience init?(color: UIColor, size: CGSize, leftCornersRadii: CGFloat = 0, rightCornersRadii: CGFloat = 0) {
        let rect = CGRect(origin: .zero, size: size)
        var fillPath: UIBezierPath
        if leftCornersRadii > 0 && rightCornersRadii <= 0 {
            fillPath = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .bottomLeft], cornerRadii: CGSize(width: leftCornersRadii, height: leftCornersRadii))
        } else if rightCornersRadii > 0  && leftCornersRadii <= 0 {
            fillPath = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topRight, .bottomRight], cornerRadii: CGSize(width: rightCornersRadii, height: rightCornersRadii))
        } else if leftCornersRadii > 0 && rightCornersRadii > 0 {
            let cornerRadius = max(leftCornersRadii, rightCornersRadii)
            fillPath = UIBezierPath(roundedRect: rect, byRoundingCorners: [.topLeft, .bottomLeft, .topRight, .bottomRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        } else {
            fillPath = UIBezierPath(rect: rect)
        }
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        fillPath.fill()
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
    
}

extension UIColor {
    convenience init(rgb: Int) {
        self.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgb & 0x0000FF) / 255.0, alpha:1.0)
    }
    
    convenience init(rgba: Int64) {
        self.init(red: CGFloat((rgba & 0xFF000000) >> 24) / 255.0, green: CGFloat((rgba & 0x00FF0000) >> 16) / 255.0, blue: CGFloat((rgba & 0x0000FF00) >> 8) / 255.0, alpha: CGFloat(rgba & 0x000000FF) / 255.0)
    }
    
    static func randomColor() -> UIColor {
        let randomRed:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        let randomGreen:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        let randomBlue:CGFloat = CGFloat(arc4random()) / CGFloat(UInt32.max)
        return UIColor(red: randomRed, green: randomGreen, blue: randomBlue, alpha: 1.0)
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
