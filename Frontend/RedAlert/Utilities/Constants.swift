//
//  Constants.swift
//  JiriOS
//
//  Created by Florin Voicu on 8/31/16.
//  Copyright © 2016 DB Global Technology. All rights reserved.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import Foundation
import UIKit

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

//
//    Mid Gray (ex. font for Thursday 10 Nov): #666666 ; Light Gray (ex: separators) #999999 ;
//    
//    [9:59]
//    Gradient 1 from #403854 ; to #675b87 ;
//    
//    [10:00]
//    Gradient 2 from #52254c ; to #853c7b ;
    
public struct BaseColors {
    static let lightViolet = UIColor(rgb: 0x853c7b)
    static let darkViolet = UIColor(rgb: 0x675b87)
    static let green = UIColor(rgb: 0x11beb2)
    static let midGray = UIColor(rgb: 0x666666)//fonts
    static let lightGray = UIColor(rgb: 0x999999)//separators, backgrounds
    static let darkVioletGradientTop = UIColor(rgb: 0x403854)
    static let darkVioletGradientBottom = UIColor(rgb: 0x675b87)
    static let lightVioletGradientTop = UIColor(rgb: 0x52254c)
    static let lightVioletGradientBottom = UIColor(rgb: 0x853c7b)
}

public enum NetworkCallsErrorTexts : String{
    case NoData = "Failed to retrieve data"
    case EncodeEndPoint = "Failed to encode endpoint"
    case FailedAuth = "Authentication failed. Please check your credentials!"
    
    case UnkownError = "An unknown error occurred"
}

public enum NetworkCallsErrorCodes : Int{
    case NoData = -10000
    case EncodeEndPoint = -10001
    case FailedAuth = -10002
    
    case UnkownError = -10003
}

public enum ErrorDataType: String {
    case text = "text"
    case code = "code"
}

public enum Status: String {
    case Success = "Success"
    case Failed = "Failed"
}

public enum TabBarControllers: Int {
    case Negatives = 0
    case Actions = 1
    case Positives = 2
}

public enum PositiveEmotions : String {
    case confident = "confident_key"
    case delighted = "delighted_key"
    case happy = "happy_key"
    case calm = "calm_key"
    case lucky = "lucky_key"
    case cheerful = "cheerful_key"
    case proud = "proud_key"
    case brave = "brave_key"
    case determined = "determined_key"
    case surprized_p = "surprized_p_key"
    case interested = "interested_key"
    
    static let positiveEmotionsArray = [calm.rawValue, cheerful.rawValue, confident.rawValue, happy.rawValue, surprized_p.rawValue]
}

public enum NegativeEmotions : String {
    case disgusted = "disgusted_key"
    case nervous = "nervous_key"
    case angry = "angry_key"
    case cross = "cross_key"
    case lonely = "lonely_key"
    case disappointed = "disappointed_key"
    case uncertain = "uncertain_key"
    case guilty = "guilty_key"
    case surprized_n = "surprized_n_key"
    case tired = "tired_key"
    case stressed = "stressed_key"
    case sad = "sad_key"
    case scared = "scared_key"
    case upset = "upset_key"
    case anxious = "anxious_key"
    case frightened = "frightened_key"
    case worried = "worried_key"
    
    static let negativeEmotionsArray = [angry.rawValue, anxious.rawValue, sad.rawValue, scared.rawValue, worried.rawValue]
    
}

public enum Actions : String {
    case stressBall = "stressBall_key"
    case deepBreathing = "deepBreathing_key"
    case haveCompany = "haveCompany_key"
    case askForHelp = "askForHelp_key"
    case timeAlone = "timeAlone_key"
    case listenMusic = "listenMusic_key"
    case browsePhotos = "browsePhotos_key"
    case count10 = "count10_key"
    case happyThoughts = "happyThoughts_key"
    case thinkPositive = "thinkPositive_key"
    case exercise = "exercise_key"
    case game = "game_key"
    
    static let actionsArray = [deepBreathing, askForHelp, listenMusic, count10, happyThoughts, thinkPositive, game]
    
    static func getEmotionActions(emotion: String) -> [Actions]{
        switch emotion {
        case NegativeEmotions.disgusted.rawValue:
            return [Actions]()
        case NegativeEmotions.worried.rawValue:
            return [Actions.game]
        case NegativeEmotions.angry.rawValue:
            return [/*Actions.stressBall, */Actions.deepBreathing]
        case NegativeEmotions.cross.rawValue:
            return [Actions]()
        case NegativeEmotions.lonely.rawValue:
            return [Actions.haveCompany]
        case NegativeEmotions.disappointed.rawValue:
            return [Actions]()
        case NegativeEmotions.uncertain.rawValue:
            return [Actions.askForHelp]
        case NegativeEmotions.guilty.rawValue:
            return [Actions]()
        case NegativeEmotions.surprized_n.rawValue:
            return [Actions]()
        case NegativeEmotions.tired.rawValue:
            return [Actions]()
        case NegativeEmotions.stressed.rawValue:
            return [Actions.stressBall, Actions.timeAlone]
        case NegativeEmotions.sad.rawValue:
            return [Actions.listenMusic/*, Actions.exercise, Actions.browsePhotos*/]
        case NegativeEmotions.scared.rawValue:
            return [Actions.deepBreathing, Actions.askForHelp]
        case NegativeEmotions.upset.rawValue:
            return [Actions]()
        case NegativeEmotions.anxious.rawValue:
            return [Actions.deepBreathing/*, Actions.count10, Actions.thinkPositive, Actions.happyThoughts*/]
        case NegativeEmotions.frightened.rawValue:
            return [Actions]()
        case NegativeEmotions.worried.rawValue:
            return [Actions]()
        default:
            return [Actions]()
        }
    }
}



//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
