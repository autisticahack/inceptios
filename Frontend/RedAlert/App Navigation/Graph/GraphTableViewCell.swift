//
//  GraphTableViewCell.swift
//  RedAlert
//
//  Created by Radu Dan on 11/10/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import UIKit

class GraphTableViewCell: UITableViewCell {
    
    @IBOutlet weak var emoticonImageView: UIImageView!
    @IBOutlet weak var feelingLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var dateFormatter: DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE dd MMMM - K:mm a"
        return dateFormatter
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadData(date: Date, emotion: String, isNegative: Bool) {
        let attributedText = NSMutableAttributedString()
        let attrs1:[String:AnyObject] = [NSFontAttributeName : UIFont.systemFont(ofSize: 14.0)]
        let attrs2:[String:AnyObject] = [NSFontAttributeName : UIFont.boldSystemFont(ofSize: 14.0)]
        let normalString = NSMutableAttributedString(string:"You felt ", attributes:attrs1)
        let boldString = NSMutableAttributedString(string:"\(emotion.uppercased())", attributes:attrs2)
        attributedText.append(normalString)
        attributedText.append(boldString)
        
        feelingLabel.attributedText = attributedText
        dateLabel.text = self.dateFormatter.string(from: date)
        emoticonImageView.image = UIImage(named: isNegative ? "smiley_sad" : "smiley_happy")
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
