//
//  CompletionQuestionViewController.swift
//  RedAlert
//
//  Created by Mihai Betej on 11/10/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit

class CompletionQuestionViewController: UIViewController {

    @IBOutlet weak var groupView: UIView!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        groupView.backgroundColor = UIColor.white
        
        yesButton.setBackgroundImage(UIImage(color: BaseColors.green , size: yesButton.frame.size), for: .normal)
        yesButton.layer.masksToBounds = true
        yesButton.layer.cornerRadius = 6
        noButton.setBackgroundImage(UIImage(color: BaseColors.lightGray , size: yesButton.frame.size), for: .normal)
        noButton.layer.masksToBounds = true
        noButton.layer.cornerRadius = 6
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func noAction(_ sender: AnyObject) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
