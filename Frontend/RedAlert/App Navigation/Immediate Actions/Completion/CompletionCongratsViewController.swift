//
//  CompletionCongratsViewController.swift
//  RedAlert
//
//  Created by Mihai Betej on 11/10/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit

class CompletionCongratsViewController: UIViewController {

    @IBOutlet weak var awsomeLabel: UILabel!
    @IBOutlet weak var closeButtonOutlet: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        closeButtonOutlet.setBackgroundImage(UIImage(color: BaseColors.darkViolet, size: closeButtonOutlet.frame.size), for: .normal)
        closeButtonOutlet.setTitleColor(UIColor.white, for: .normal)
        closeButtonOutlet.layer.masksToBounds = true
        closeButtonOutlet.layer.cornerRadius = 6
        awsomeLabel.textColor = BaseColors.darkViolet
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func closeAction(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
