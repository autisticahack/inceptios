//
//  ImmediateActionsViewController.swift
//  RedAlert
//
//  Created by Mihai Betej on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import UIKit

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class ImmediateActionsViewController: UIViewController {

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Vars
    
    @IBOutlet var immediateActionButtons: [UIButton]!
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.title = "Relax"
        self.customizeAppearance()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Class utils
    
    class func showImmediateActions(presentingController: UIViewController) {
        let currentEmotion = Settings.value(forKey: DefaultsKeys.selectedEmotionKey)
        
        guard let action = Actions.getEmotionActions(emotion: currentEmotion as! String).first else {
            return
        }
        
        let immediateActionsStoryboard = UIStoryboard(name: "ImmediateActionsStoryboard", bundle: nil)
        var initialViewController: UIViewController?
        var navController: UINavigationController?
        
        switch action {
        case Actions.game:
            initialViewController = immediateActionsStoryboard.instantiateViewController(withIdentifier: "PlayViewController")
            navController = UINavigationController(rootViewController: initialViewController!)
        case Actions.deepBreathing:
            initialViewController = immediateActionsStoryboard.instantiateViewController(withIdentifier: "BreatheViewController")
            navController = UINavigationController(rootViewController: initialViewController!)
            navController!.setNavigationBarHidden(true, animated: false)
        case Actions.listenMusic:
            initialViewController = immediateActionsStoryboard.instantiateViewController(withIdentifier: "MusicViewController")
            navController = UINavigationController(rootViewController: initialViewController!)
        default:
            return
        }
        
        if navController != nil {
            presentingController.present(navController!, animated: true, completion: nil)
        }        
    }
    
    private func customizeAppearance() {
        self.view.backgroundColor = UIColor.randomColor()        
        for immediateActionButton in immediateActionButtons {
            immediateActionButton.setBackgroundImage(UIImage(color: UIColor.randomColor(), size: immediateActionButton.frame.size), for: .normal)
            immediateActionButton.setTitleColor(UIColor.white, for: .normal)
        }
        
        self.navigationController?.navigationItem.hidesBackButton = true
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Actions
    
    @IBAction func doneAction(_ sender: AnyObject) {
        dismiss(animated: true, completion: nil)
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
