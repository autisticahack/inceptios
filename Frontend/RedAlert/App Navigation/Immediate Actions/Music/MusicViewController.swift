//
//  MusicViewController.swift
//  RedAlert
//
//  Created by Mihai Betej on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import UIKit
import MediaPlayer

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class MusicViewController: UIViewController {

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Vars
    // Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrubControlsContainer: UIView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var nextItemButton: UIButton!
    @IBOutlet weak var nextItemButtonTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var previousItemButton: UIButton!
    @IBOutlet weak var previousItemButtomLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrubSlider: UISlider!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var playingLabel: UILabel!
    // Other
    fileprivate let audioPlayer = AVPlayer()
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        title = "Music"
        tableView.backgroundColor = UIColor.white//BaseColors.darkViolet
        self.navigationController?.navigationItem.hidesBackButton = true
        addBlurrEffectOnScrubView()
        updateMusicPlayer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

//        previousItemButtomLeadingConstraint.constant = (scrubControlsContainer.frame.width / 2 - playPauseButton.frame.width / 2 - previousItemButton.frame.width / 2) / 2
//        nextItemButtonTrailingConstraint.constant = (scrubControlsContainer.frame.width / 2 - playPauseButton.frame.width / 2 - nextItemButton.frame.width / 2) / 2
        tableView.contentInset = UIEdgeInsetsMake(0, 0, scrubControlsContainer.frame.height + 16, 0)
    }
    
    deinit {
        audioPlayer.pause()
        audioPlayer.replaceCurrentItem(with: nil)
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Class utils
    
    private func addBlurrEffectOnScrubView() {
        scrubControlsContainer.backgroundColor = .clear
        
        // Blur effect
        let blurEffect = UIBlurEffect(style: .light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = scrubControlsContainer.bounds
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        scrubControlsContainer.insertSubview(blurView, at: 0)
        
        // Vibrancy
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
        //vibrancyView.translatesAutoresizingMaskIntoConstraints = false
        vibrancyView.frame = blurView.bounds
        vibrancyView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.contentView.addSubview(vibrancyView)
        
        scrubControlsContainer.layer.masksToBounds = true
        scrubControlsContainer.layer.cornerRadius = 8
    }
    
    fileprivate func updateMusicPlayer() {
        let enabled = DataManager.sharedInstance.getMediaItems().count > 0
        previousItemButton.isEnabled = enabled
        nextItemButton.isEnabled = enabled
        playPauseButton.isEnabled = enabled
        scrubSlider.isEnabled = enabled
        
        playingLabel.text = audioPlayer.currentItem != nil ? DataManager.sharedInstance.mediaItem(for: audioPlayer.currentItem!.asset as! AVURLAsset).title : "-"
    }
    
    private func updateProgressBar(item: AVPlayerItem) {
        let duration = CMTimeGetSeconds(item.duration)
        let time = CMTimeGetSeconds(audioPlayer.currentTime())
        scrubSlider.value = Float(time / duration)
    }
    
    private func updateDuration(item: AVPlayerItem) {
        let duration = CMTimeGetSeconds((item.asset as! AVURLAsset).duration)
        
        let secsBellowOneMin = Int(duration) % 60
        let multipleMinDuration = Int(duration) - secsBellowOneMin
        let minutes = multipleMinDuration / 60
        
        let strMin = minutes < 10 ? "0\(minutes)" : "\(minutes)"
        let strSec = secsBellowOneMin < 10 ? "0\(secsBellowOneMin)" : "\(secsBellowOneMin)"
        
        durationLabel.text = "\(strMin):\(strSec)"
    }

    private func updateCurrentTime(item: AVPlayerItem) {
        let currentTime = CMTimeGetSeconds(item.currentTime())
        
        let secsBellowOneMin = Int(currentTime) % 60
        let multipleMinDuration = Int(currentTime) - secsBellowOneMin
        let minutes = multipleMinDuration / 60
        
        let strMin = minutes < 10 ? "0\(minutes)" : "\(minutes)"
        let strSec = secsBellowOneMin < 10 ? "0\(secsBellowOneMin)" : "\(secsBellowOneMin)"
        
        currentTimeLabel.text = "\(strMin):\(strSec)"
    }
    
    private func getSongs() {
        if #available(iOS 9.3, *) {
            if MPMediaLibrary.authorizationStatus() == MPMediaLibraryAuthorizationStatus.notDetermined {
                self.requestMediaAccess(completion: { (authorized) in
                    if authorized {
                        self.presentMediaPicker()
                    }
                })
            } else if MPMediaLibrary.authorizationStatus() == MPMediaLibraryAuthorizationStatus.authorized {
                self.presentMediaPicker()
            }
        } else {
            // Fallback on earlier versions
            self.presentMediaPicker()
        }
    }
    
    private func requestMediaAccess(completion: @escaping ((Bool) -> Void)) {
        if #available(iOS 9.3, *) {
            MPMediaLibrary.requestAuthorization { (status) in
                if status == .authorized {
                    completion(true)
                } else {
                    completion(false)
                }
            }
        } else {
            // Fallback on earlier versions
            self.presentMediaPicker()
        }
    }

    private func presentMediaPicker() {        
        let mp = MPMediaPickerController(mediaTypes: MPMediaType.any)
        mp.allowsPickingMultipleItems = true
        mp.showsCloudItems = false
        mp.delegate = self
        present(mp, animated: true, completion: nil)
    }
    
    fileprivate func play(item: MPMediaItem) {
        let avPlayerItem = AVPlayerItem(url: item.assetURL!)
        audioPlayer.replaceCurrentItem(with: avPlayerItem)
        audioPlayer.play()
        playPauseButton.setImage(UIImage(named: "pause-btn-icon"), for: .normal)
        updateDuration(item: avPlayerItem)
        playingLabel.text = DataManager.sharedInstance.mediaItem(for: audioPlayer.currentItem!.asset as! AVURLAsset).title
        
        audioPlayer.addPeriodicTimeObserver(forInterval:  CMTime(seconds: 1.0 / 60, preferredTimescale: CMTimeScale(NSEC_PER_SEC)), queue: nil) { [weak self] (time) in
            if let currentSelf = self {
                if let currentItem = currentSelf.audioPlayer.currentItem {
                    currentSelf.updateProgressBar(item: currentItem)
                    currentSelf.updateCurrentTime(item: currentItem)
                }
            }
        }
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Actions

    @IBAction func addMusicAction(_ sender: AnyObject) {
        //addItunesLibraryItems()
        getSongs()
    }
    
    @IBAction func goBack(_ sender: AnyObject) {
        //addItunesLibraryItems()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func playPauseAction(_ sender: AnyObject) {
        if audioPlayer.rate == 1 { // Playing
            audioPlayer.pause()
            playPauseButton.setImage(UIImage(named: "play-btn-icon"), for: .normal)
        } else {
            if audioPlayer.currentItem == nil {
                if let firstItem = DataManager.sharedInstance.getMediaItems().first {
                    play(item: firstItem)
                }
            } else {
                audioPlayer.play()
                playPauseButton.setImage(UIImage(named: "pause-btn-icon"), for: .normal)
            }
        }
    }

    @IBAction func previousItemAction(_ sender: AnyObject) {
        if let currentItem = audioPlayer.currentItem {
            let prevMediaItem = DataManager.sharedInstance.prevMediaItem(before: currentItem.asset as! AVURLAsset)
            play(item: prevMediaItem)
        }
    }

    @IBAction func nextItemAction(_ sender: AnyObject) {
        if let currentItem = audioPlayer.currentItem {
            let nextMediaItem = DataManager.sharedInstance.nextMediaItem(after: currentItem.asset as! AVURLAsset)
            play(item: nextMediaItem)
        }
    }
    
    
    @IBAction func scrubSliderValueChanged(_ sender: AnyObject) {
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - MusicViewController(MPMediaPickerControllerDelegate)

extension MusicViewController: MPMediaPickerControllerDelegate {
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        print("mp cancel")
        dismiss(animated: true, completion: nil)
    }
    
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        // Update local music lib data source and reload data
        DataManager.sharedInstance.addMediaItems(newMediaItems: mediaItemCollection.items)
        tableView.reloadData()
        updateMusicPlayer()
        // Make sure picker is dismissed
        dismiss(animated: true, completion: nil)
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - MusicViewController(UITableViewController)

extension MusicViewController: UITableViewDataSource, UITableViewDelegate {
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("tableView no of items \(DataManager.sharedInstance.getMediaItems().count)")
        return DataManager.sharedInstance.getMediaItems().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "music-item-cell", for: indexPath) as! MusicItemCell
        cell.configure(mediaItem: DataManager.sharedInstance.getMediaItems()[indexPath.row], even: indexPath.row % 2 == 0)
        
        return cell
    }
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Delegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // play this song
        let mediaItem = DataManager.sharedInstance.getMediaItems()[indexPath.row]
        play(item: mediaItem)
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - MusicItemCell

class MusicItemCell: UITableViewCell {
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Vars
    
    @IBOutlet weak var albumArtImageView: UIImageView!
    @IBOutlet weak var songTitleLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        songTitleLabel.textColor = BaseColors.darkViolet
        artistNameLabel.textColor = BaseColors.darkViolet
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Configure
    
    func configure(mediaItem: MPMediaItem, even: Bool) {
        self.albumArtImageView?.image = mediaItem.artwork?.image(at: albumArtImageView.frame.size)
        self.songTitleLabel.text = mediaItem.title
        self.artistNameLabel.text = mediaItem.albumArtist
        
        self.backgroundColor = even ? UIColor(rgb: 0xeeeeee) : UIColor.white
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
