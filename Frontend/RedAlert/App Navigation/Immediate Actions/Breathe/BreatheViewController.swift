//
//  BreatheViewController.swift
//  RedAlert
//
//  Created by Mihai Betej on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

import UIKit
import AVKit
import AVFoundation

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

class BreatheViewController: UIViewController {

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Vars
    
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var stopButton: UIButton!

    private var player: AVPlayer!
    private let playTime: TimeInterval = 2 * 60
    private var currentPlayTime: TimeInterval = 0
    private var countDownTimer: Timer!
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.title = "Breathe"
        self.navigationController?.navigationItem.hidesBackButton = true
        self.addMediaPlayer()
        self.customizeAppearance()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        player.play()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if countDownTimer != nil {
            countDownTimer.invalidate()
            countDownTimer = nil
        }
        
        player.pause()
        let targetTime = CMTimeMakeWithSeconds(0, Int32(NSEC_PER_SEC));
        player.seek(to: targetTime)
        
        currentPlayTime = playTime
        self.updateCounterLabel()
    }
    
    deinit {
        print("deinit")
        NotificationCenter.default.removeObserver(self)
        player.removeObserver(self, forKeyPath: "status")
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Class utils
    
    private func addMediaPlayer() {
        //let videoURL = NSURL(string: "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4")
        let videoURL = NSURL(string: "http://10.0.179.14:8080/videos/breathe")
        player = AVPlayer(url: videoURL! as URL)
        
        player.actionAtItemEnd = .none
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd(_:)), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        player.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions.new, context: nil)
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        self.view.layer.addSublayer(playerLayer)
    }
    
    private func customizeAppearance() {
        playButton.setBackgroundImage(UIImage(color: BaseColors.darkViolet, size: playButton.frame.size), for: .normal)
        playButton.setTitleColor(UIColor.white, for: .normal)
        playButton.layer.masksToBounds = true
        playButton.layer.cornerRadius = 6
        
        currentPlayTime = playTime
        updateCounterLabel()
    }
    
    private func updateCounterLabel() {
        let secsBellowOneMin = Int(currentPlayTime) % 60
        let multipleMinDuration = Int(currentPlayTime) - secsBellowOneMin
        let minutes = multipleMinDuration / 60
        
        let strMin = minutes < 10 ? "0\(minutes)" : "\(minutes)"
        let strSec = secsBellowOneMin < 10 ? "0\(secsBellowOneMin)" : "\(secsBellowOneMin)"
        
        timerLabel.text = "\(strMin):\(strSec)"
    }
    
    private func startCountDown() {
        if countDownTimer == nil {
            currentPlayTime = playTime
            updateCounterLabel()
            countDownTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDown(_:)), userInfo: nil, repeats: true)
        }
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Actions
    
    func countDown(_ timer: Timer) {
        currentPlayTime = currentPlayTime - 1
        if currentPlayTime == 0 {
            countDownTimer.invalidate()
            countDownTimer = nil
            
            // Go to next screen
            self.performSegue(withIdentifier: "showCompletionScreen", sender: nil)
        }
        
        updateCounterLabel()
    }

    @IBAction func playPauseAction(_ sender: AnyObject) {
        if player.rate == 1 {
            player.pause()
        }
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: Notification handler
    
    func playerItemDidReachEnd(_ notification: NSNotification) {
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: kCMTimeZero)
        }
    }
    
    
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // MARK: KVO
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "status" && object as? AVPlayer == player {
            if player.status == AVPlayerStatus.readyToPlay {
                DispatchQueue.main.async {
                    self.playButton.alpha = 0
                    self.playButton.isHidden = false
                    UIView.animate(withDuration: 0.5, animations: { 
                        self.playButton.alpha = 1
                    }, completion: { (finished) in
                        self.startCountDown()
                    })
                }
            }
        }
    }
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
