//
//  ActionCollectionViewCell.swift
//  RedAlert
//
//  Created by Florin Voicu on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit

class ActionCollectionViewCell: UICollectionViewCell {
    //outlets
    @IBOutlet weak var actionLabel: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    //class properties
    var action : String?
    
    func configureCell(withAction:String?){
        if let action = withAction{
            self.action = action
//            let labelString = NSMutableAttributedString()
//            
//            let actionText = NSLocalizedString(action+"_title", comment: "no_action_found")
//            
//            var titleAttributes = [String : Any]()
//            titleAttributes[NSForegroundColorAttributeName] = UIColor.green
//            titleAttributes[NSUnderlineStyleAttributeName] = NSUnderlineStyle.styleDouble.rawValue
//            
//            let titleAttributtedString = NSMutableAttributedString(string: actionText + "\n", attributes: titleAttributes)
//            labelString.append(titleAttributtedString)
//            
//            let descriptionText = NSLocalizedString(action+"_description", comment: "no_action_found")
//            
//            var descAttributes = [String : Any]()
//            descAttributes[NSForegroundColorAttributeName] = UIColor.red
//            descAttributes[NSUnderlineStyleAttributeName] = NSUnderlineStyle.styleDouble.rawValue
//            
//            let descriptionAttributedString = NSAttributedString(string: descriptionText, attributes: descAttributes)
//            labelString.append(descriptionAttributedString)
//            
//            actionLabel.attributedText = labelString
            
            backgroundImage.image = UIImage(named: action)
        }
    }
    
    func selectCell() {
        if let action = self.action {
            backgroundImage.image = UIImage(named: action+"_selected")
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                // your code here
                self.backgroundImage.image = UIImage(named: action)
            }
        }
    }
}
