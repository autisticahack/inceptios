//
//  BaseTableViewController.swift
//  RedAlert
//
//  Created by Florin Voicu on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit

let EmotionCellIdentifier = "EmotionCell"
let iPhonePlusCellHeight : CGFloat = 138.0
let iPhoneCellHeight : CGFloat = 109.0

class BaseTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    //storyboard outlets
    @IBOutlet weak var myTableView: UITableView!
    var gl: CAGradientLayer?
    
    //class properties
    var emotionsArray = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.myTableView.delegate = self
        self.myTableView.dataSource = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - IBActions
    
    @IBAction func showStats(_ sender: UIButton) {
        
    }
    
    
    // MARK: - Customization
    func populate(withDataSet: [String]){
        self.emotionsArray = withDataSet
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }  
    
    // MARK: - UITableViewDelegateAndDataSource
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return (UIScreen.main.nativeBounds.width/UIScreen.main.nativeScale - 40.0)*139.0/374.0
        //UIScreen.main.nativeScale == 3 ? iPhonePlusCellHeight : iPhoneCellHeight
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emotionsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: EmotionCellIdentifier, for: indexPath) as! EmotionTableViewCell
        
        //configure cell
        cell.configureCell(withEmotion: emotionsArray[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let emotion = emotionsArray[indexPath.row] as String? {
            Settings.write(value: emotion, forKey: DefaultsKeys.selectedEmotionKey)
            //self.tabBarController?.selectedIndex = TabBarControllers.Actions.rawValue
            ImmediateActionsViewController.showImmediateActions(presentingController: self)
        }
    }
    
}
