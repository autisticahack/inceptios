//
//  ActionsViewController.swift
//  RedAlert
//
//  Created by Florin Voicu on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit

let ActionCellIdentifier = "ActionCell"
let iPhonePlusCellSize = CGSize(width: 177, height: 177)
let iPhoneCellSize = CGSize(width: 130, height: 130)

class ActionsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //Outlets
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    //Class properties
    var actionsArray = [Actions]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.myCollectionView.delegate = self
        self.myCollectionView.dataSource = self

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let emotion = Settings.value(forKey: DefaultsKeys.selectedEmotionKey) as? String{
            self.actionsArray = Actions.getEmotionActions(emotion: emotion)
        } else {
            self.actionsArray =  Actions.actionsArray
        }
        self.myCollectionView.reloadData()
    }
    
    // MARK: - Customize
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - IBActions
    
    @IBAction func showStats(_ sender: UIButton) {

        
    }
    

    // MARK: - UICollectionViewDelegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //present the action he selected
        if let cell = collectionView.cellForItem(at: indexPath) as? ActionCollectionViewCell {
            cell.selectCell()
        }
    }
    
    // MARK: - UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return actionsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ActionCellIdentifier, for: indexPath) as! ActionCollectionViewCell

        //configuration
        cell.configureCell(withAction: actionsArray[indexPath.row].rawValue)
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.nativeBounds.width/UIScreen.main.nativeScale - 60.0)/2.0, height: (UIScreen.main.nativeBounds.width/UIScreen.main.nativeScale - 60.0)/2.0)
//        return UIScreen.main.nativeScale > 2 ? iPhonePlusCellSize : iPhoneCellSize
    }
}
