//
//  NegativesViewController.swift
//  RedAlert
//
//  Created by Florin Voicu on 11/9/16.
//  Copyright © 2016 Florin Voicu. All rights reserved.
//

import UIKit

class NegativesViewController: BaseTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.populate(withDataSet: NegativeEmotions.negativeEmotionsArray)
        gl = CAGradientLayer()
        gl?.colors = [ BaseColors.lightVioletGradientTop.cgColor, BaseColors.lightVioletGradientBottom.cgColor]
        gl?.locations = [ 0.0, 1.0]
        gl?.startPoint = CGPoint(x: 0.0, y: 0.0)
        gl?.endPoint = CGPoint(x: 0.0, y: 1.0)
        
        gl?.frame = CGRect(x: 0.0, y: 0.0, width: UIScreen.main.nativeBounds.width/UIScreen.main.nativeScale, height: UIScreen.main.nativeBounds.height/UIScreen.main.nativeScale)
        
        self.view.layer.insertSublayer(gl!, at: 0)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
