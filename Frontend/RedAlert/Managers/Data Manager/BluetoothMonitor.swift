//
//  BluetoothMonitor.swift
//  RedAlert
//
//  Created by Cristiana Dogaru on 11/10/16.
//  Copyright © 2016 DB Global Technologies. All rights reserved.
//

import UIKit
import CoreBluetooth
import UserNotifications

public let POLARH7_HRM_DEVICE_INFO_SERVICE_UUID = "180A"// 180A = Device Information
public let POLARH7_HRM_HEART_RATE_SERVICE_UUID = "180D" // 180D = Heart Rate Service
public let POLARH7_HRM_ENABLE_SERVICE_UUID = "2A39"
public let POLARH7_HRM_NOTIFICATIONS_SERVICE_UUID = "2A37"
public let POLARH7_HRM_BODY_LOCATION_UUID = "2A38"
public let POLARH7_HRM_MANUFACTURER_NAME_UUID = "2A29"

class BluetoothMonitor: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    
    let heartRateManager = HeartRateMonitor()
    
    var centralManager: CBCentralManager!
    var polarH7HRMPeripheral: CBPeripheral!
    
    // Properties to hold data characteristics for the peripheral device
    var connected : String?
    var bodyData : String?
    var manufacturer : String?
    var polarH7DeviceData :String?
    var heartRate : UInt16 = 0
    let services = [CBUUID.init(string: POLARH7_HRM_HEART_RATE_SERVICE_UUID), CBUUID.init(string: POLARH7_HRM_DEVICE_INFO_SERVICE_UUID)]
    
    var writeTimer : Timer?
    var arrayOfRecords = [Double]()
    
    
    override init() {
        super.init()
        centralManager = CBCentralManager(delegate: self, queue: nil, options:nil)
        let services = [CBUUID(string: POLARH7_HRM_HEART_RATE_SERVICE_UUID), CBUUID(string: POLARH7_HRM_DEVICE_INFO_SERVICE_UUID)]
        centralManager.scanForPeripherals(withServices: services, options: nil)
    }
    
    func openHeartRateMonitor() {
        self.heartRateManager.firstTimeRate = true
        self.heartRateManager.setUpBackgroundDelivery()
        
        
    }
    
    // MARK: CB Delegate Methods
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        // Determine the state of the peripheral
        if central.state == .poweredOff {
            print("CoreBluetooth BLE hardware is powered off")
        }
        else if central.state == .poweredOn {
            print("CoreBluetooth BLE hardware is powered on and ready")
            self.centralManager.scanForPeripherals(withServices: services, options: nil)
        }
    }
    
    // method called whenever we have successfully connected to the BLE peripheral
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        peripheral.delegate = self
        peripheral.discoverServices(nil)
        connected = String("Connected: \(peripheral.state.rawValue)")
    }
    
    // CBPeripheralDelegate - Invoked when you discover the peripheral's available services.
    func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
        for service in peripheral.services!{
            peripheral.discoverCharacteristics(nil, for: service)
        }
    }
    
    // CBCentralManagerDelegate - This is called with the CBPeripheral class as its main input parameter. This contains most of the information there is to know about a BLE peripheral.
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        let localModel = advertisementData["CBAdvertisementDataLocalNameKey"] as! String?
        
        if localModel != "" {
            print(localModel)
            self.centralManager.stopScan()
            self.polarH7HRMPeripheral = peripheral
            self.polarH7HRMPeripheral.delegate = self
            self.centralManager.connect(peripheral, options: nil)
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
        
        if service.uuid == CBUUID.init(string: POLARH7_HRM_HEART_RATE_SERVICE_UUID) {  // 1
            for char in service.characteristics!{
                // Request heart rate notifications
                if char.uuid == CBUUID.init(string: POLARH7_HRM_NOTIFICATIONS_SERVICE_UUID) { // 2
                    self.polarH7HRMPeripheral.setNotifyValue(true, for: char)
                } else if char.uuid == CBUUID.init(string: POLARH7_HRM_BODY_LOCATION_UUID) { // 3
                    self.polarH7HRMPeripheral.readValue(for: char)
                } else if char.uuid == CBUUID.init(string: POLARH7_HRM_ENABLE_SERVICE_UUID) { //4
                    var value = 0x01 as UInt8
                    let data = Data.init(bytes: &value, count: Int(value.toIntMax()))
                    peripheral.writeValue(data, for: char, type: .withResponse)
                }
            }
        }
        // Retrieve Device Information Services for the Manufacturer Name
        if service.uuid == CBUUID.init(string: POLARH7_HRM_DEVICE_INFO_SERVICE_UUID) { //5
            for char in service.characteristics! {
                if char.uuid == CBUUID.init(string: POLARH7_HRM_MANUFACTURER_NAME_UUID) {
                    self.polarH7HRMPeripheral.readValue(for: char)
                    print("Found a Device Manufacturer Name Characteristic")
                }
            }
        }
    }
    
    func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
        if characteristic.uuid.isEqual(CBUUID(string: POLARH7_HRM_NOTIFICATIONS_SERVICE_UUID)) {
            self.getHeartBPMData(characteristic: characteristic, error: nil)
        }
        // Retrieve the characteristic value for manufacturer name received
        if characteristic.uuid.isEqual(CBUUID(string: POLARH7_HRM_MANUFACTURER_NAME_UUID)) {
            // 2
            self.getManufacturerName(characteristic: characteristic)
        }
        else if characteristic.uuid.isEqual(CBUUID(string: POLARH7_HRM_BODY_LOCATION_UUID)) {
            // 3
            self.getBodyLocation(characteristic: characteristic)
        }
    }
    
    
    func getHeartBPMData(characteristic: CBCharacteristic , error: NSError? ){
        
        let data = characteristic.value
        //let reportData = UnsafePointer<UInt8>(data?.bytes)
        
        var reportData = data?.withUnsafeBytes {
            [UInt8](UnsafeBufferPointer(start: $0, count: (data?.count)!))
        }
        
        
        var bpm : UInt16
        if (reportData![0] & 0x01) == 0 {
            bpm = UInt16(reportData![1])
        } else {
            bpm = CFSwapInt16LittleToHost(UInt16((reportData?[1])!))
        }
        
        let outputString = String(bpm)
        
        self.arrayOfRecords.append(Double(outputString)!)
         self.writeTimer = Timer.scheduledTimer(timeInterval: 20.0, target: self, selector: #selector(BluetoothMonitor.count), userInfo: nil, repeats: true)
        
        print("De la bratara heart rate: \(outputString)")
    }
    
    // Instance method to get the manufacturer name of the device
    func getManufacturerName(characteristic: CBCharacteristic)
    {
        let manufacturerName = String(describing: characteristic.value)
        self.manufacturer = manufacturerName
        
    }
    
    // Instance method to get the body location of the device
    func getBodyLocation(characteristic: CBCharacteristic)
    {
        let data = characteristic.value
        //let reportData = UnsafePointer<UInt8>(data?.bytes)
        
        var bodyData = data?.withUnsafeBytes {
            [UInt8](UnsafeBufferPointer(start: $0, count: (data?.count)!))
        }
        
        if (bodyData != nil) {
            let bodyLocation: UInt8 = bodyData![0]
            self.bodyData = "Body Location: \(bodyLocation == 1 ? "Chest" : "Undefined")"
        }
        else {
            self.bodyData = "Body Location: N/A"
        }
    }
    
    
    func count(){
        let averageRecent = self.arrayOfRecords.average
        var averageHealthKit = 0.0
        self.arrayOfRecords.removeAll()
         heartRateManager.retreiveHeartRateDataFromHealthKit { (result) in
         averageHealthKit = (result?.average)!
        }
        
        if averageHealthKit > 0.0 {
            if averageRecent - averageHealthKit > 25 {
                print("Mai mare!!!")
                
                
             self.showNotification()
                
            }
            print("de la bratara \(averageRecent)")
            print ( "De la healtkit :\(averageHealthKit)")
        }
        
    }
    

    func showNotification() {
        let title = "Heart Rate"
        let body = "Are you OK?"
        
        if #available(iOS 10.0, *){
            let content = UNMutableNotificationContent()
            
            content.title = title
            content.body = body
            content.sound = UNNotificationSound.default()
            
            // Deliver the notification in five seconds.
            let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 5, repeats: false)
            let request = UNNotificationRequest.init(identifier: "FiveSecond", content: content, trigger: trigger)
            
            // Schedule the notification.
            let center = UNUserNotificationCenter.current()
            center.add(request) { (error) in
                print(error?.localizedDescription)
            }
            print("should have been added")
        } else {
            let notification = UILocalNotification()
            notification.alertTitle = title
            notification.alertBody =  body// text that will be displayed in the notification
            notification.alertAction = "open" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
            
            UIApplication.shared.scheduleLocalNotification(notification)
        }
    }

}

extension Array where Element: FloatingPoint {
    /// Returns the sum of all elements in the array
    var total: Element {
        return reduce(0, +)
    }
    /// Returns the average of all elements in the array
    var average: Element {
        return isEmpty ? 0 : total / Element(count)
    }
}

extension Array where Element: Integer {
    /// Returns the sum of all elements in the array
    var total: Element {
        return reduce(0, +)
    }
}
extension Collection where Iterator.Element == Int, Index == Int {
    /// Returns the average of all elements in the array
    var average: Double {
        return isEmpty ? 0 : Double(reduce(0, +)) / Double(endIndex-startIndex)
    }
}
