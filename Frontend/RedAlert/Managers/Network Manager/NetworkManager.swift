//
//  NetworkManager.swift
//  NovusSpem
//
//  Created by Florin Voicu on 10/28/16.
//  Copyright © 2016 DB Global Technology. All rights reserved.
//

import UIKit

 typealias JSONCompletion = (DataResponse<Any>) -> Void
 typealias DataCompletion = (DataResponse<Data>) -> Void

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MARK: - NetworkManager

class NetworkManager:NSObject {
    /**
     Make a request that returns a JSON answer
     
     - Parameter url: A string representing the URL we want to access
     - Parameter method: HTTP method of the request
     - Parameter parameters: A dictionary with the format [String:Any]
     - Parameter username: Username for Authentication
     - Parameter password: Password for Authentication
     - Parameter completion: A handler passing the result of the request
    */
    func makeJSONRequest(_ url: URLConvertible,
                     method: HTTPMethod = .get,
                     parameters: Parameters? = nil,
                     username: String? = nil,
                     password: String? = nil,
                     completion: @escaping JSONCompletion) {
        
        var headers = HTTPHeaders()
        headers["Accept"] = "application/json"
        if username != nil && password != nil {
            headers["Authorization"] = "Basic \((username!+":"+password!).base64Encode())"
        }
        switch method {
        case .get:
            request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers)
                .validate()
                .responseJSON(completionHandler: { response in
                completion(response)
            })
        case .post:
            request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
                .validate()
                .responseJSON(completionHandler: { response in
                completion(response)
            })
        default:
            return
        }
    }
    
    /**
     Make a request that returns a JSON answer
     
     - Parameter url: A string representing the URL we want to access
     - Parameter method: HTTP method of the request
     - Parameter parameters: A dictionary with the format [String:Any]
     - Parameter username: Username for Authentication
     - Parameter password: Password for Authentication
     - Parameter completion: A handler passing the result of the request
     */
    func makeDataRequest(_ url: URLConvertible,
                     method: HTTPMethod = .get,
                     parameters: Parameters? = nil,
                     username: String? = nil,
                     password: String? = nil,
                     completion: @escaping DataCompletion) {
        
        var headers = HTTPHeaders()
        headers["Accept"] = "application/json"
        if username != nil && password != nil {
            headers["Authorization"] = "Basic \((username!+":"+password!).base64Encode())"
        }
        switch method {
        case .get:
            request(url, method: method, parameters: parameters, encoding: URLEncoding.default, headers: headers)
                .validate()
                .responseData(completionHandler: { response in
                completion(response)
            })
        case .post:
            request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
                .validate()
                .responseData(completionHandler: { response in
                completion(response)
            })
        default:
            return
        }
    }
    

}
