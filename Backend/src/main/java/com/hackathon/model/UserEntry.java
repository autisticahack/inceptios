package com.hackathon.model;

import javax.persistence.*;

@Entity
@Table(name = "user_entries")
public class UserEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ENTRY_ID")
    private Long id;

    private String timestamp;
    private String emotion;
    private Boolean isNegative;
    private String action;
    private String longitude;
    private String latitude;

    private Boolean isBetter;

    public UserEntry() {
    }

    public UserEntry(String timestamp, String emotion, Boolean isNegative, String action, String longitude, String latitude, Boolean isBetter) {
        this.timestamp = timestamp;
        this.emotion = emotion;
        this.isNegative = isNegative;
        this.action = action;
        this.longitude = longitude;
        this.latitude = latitude;
        this.isBetter = isBetter;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public Boolean getNegative() {
        return isNegative;
    }

    public void setNegative(Boolean negative) {
        isNegative = negative;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Boolean getBetter() {
        return isBetter;
    }

    public void setBetter(Boolean better) {
        isBetter = better;
    }
}
