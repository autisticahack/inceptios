package com.hackathon;

import com.hackathon.controller.EntriesController;
import com.hackathon.model.UserEntry;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class HackathonBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(HackathonBackendApplication.class, args);
    }

    @Bean
    public CommandLineRunner demoData(EntriesController entriesController) {
        return (args) -> {
            entriesController.save(Arrays.asList(
                    new UserEntry("2016-02-09T20:30:19", "lucky", false, "action #1", "44.4763905","26.1238624", false),
                    new UserEntry("2016-03-09T07:30:19", "anxious", true, "action #2", "44.3907915","26.0191154", true),
                    new UserEntry("2016-04-09T15:01:19", "dissapointed", true, "action _x", "44.3907915","26.0191154", true),
                    new UserEntry("2016-05-09T04:58:19", "calm", false, "action #3", "44.3907915","26.0191154", false),
                    new UserEntry("2016-06-09T12:00:01", "good", true, "action #4", "44.3907915","26.0191154", true)
                    )
            );
        };
    }
}
