package com.hackathon.controller;

import com.hackathon.model.UserEntry;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "entries", path = "entries")
public interface EntriesController extends PagingAndSortingRepository<UserEntry, Long>{
}
