package com.hackathon.controller;

import com.hackathon.util.MultipartFileSender;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.nio.file.Paths;

@RestController
@RequestMapping("/videos")
public class VideoController {

    @RequestMapping("/breathe")
    public void getBreatheMovie(HttpServletRequest request, HttpServletResponse response) throws Exception{
        MultipartFileSender.fromPath(Paths.get("C:\\Users\\SharedUser\\Desktop\\breathe1.mp4"))
                .with(request)
                .with(response)
                .serveResource();
    }
}
